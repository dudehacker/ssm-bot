const fs = require('fs');
const Discord = require('discord.js');
const client = new Discord.Client();
client.commands = new Discord.Collection();
var config = require("./config.json");
const idle = require('./idle.js');
const utils = require('./utils.js');

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

client.on('error', function() {
    // Relog again here
    console.log("error event");
   for (var i = 0 ; i < 3 ; i++){
       try {
        setTimeout(login, 30000);
       } catch (e){
           console.log("error reconnecting..");
           console.log(e);
       }
   }
    
});

function login(){
    console.log("connecting...");
    client.login(config.token); 
}

// Gets called when bot is successfully logged in and connected
client.on('ready', () => {
    console.log('Bot is connected!');
    // Change bot's playing game
    client.user.setActivity('Preparing matches');
});

// This event will run on every single message received, from any channel or DM.
client.on('message', async message => {
    
    // if @playing is mentioned
    const playingRole = message.mentions.roles.find('name', 'Playing');
    if (playingRole != null) {
        await idle.clearIdle(message.guild);
        if (playingRole.members.array().length === 0) {
            // no one is playing
            const msg = require("./commands/playing.js").msg;
            return message.channel.send(msg);
        }
    }
    
    // So the bot doesn't reply to iteself or other bots
    if (!message.content.startsWith(config.prefix) || message.author.bot || !utils.isValidCommand(message.content)) 
    return;

    
    // separate command and arguments from user message
    var args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const commandName = args.shift().toLowerCase();

    // handle the commands
    if (!commandName) return;
    
    var command = client.commands.get(commandName)
        || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) {
        command = client.commands.get("help");
    }

    if (command.args && !args.length){
        args = [commandName];
    }

    if (command.guildOnly && message.channel.type !== 'text') {
		return message.reply('I can\'t execute that command inside DMs!');
    }

    if (command.adminOnly){
        if (!utils.isAdmin(message.member)) {
            return message.reply(utils.PermissionDeniedException);
        }
    }

    if (command.rankerOnly){
        if (!utils.ishigherTop10(message.member)) {
            return message.reply(utils.PermissionDeniedException);
        }
    }

    try {
        command.execute(message, args);
    }
    catch (error) {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }

});



login();