module.exports = {
    name: 'stop',
    description: 'Let people know you are no longer ready to grind',
    guildOnly: true,
    execute(message) {
        const role = message.guild.roles.find('name', 'Playing');
        message.member.removeRole(role);
        return message.channel.send(`**${message.author.username}** is done grinding!`);
    },

};