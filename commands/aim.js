module.exports = {
    name: 'aim',
    description: 'Setting your aim for the event',
    guildOnly: true,
    args: true,
    usage: `1-3 | 10 | 100 | 1000 | 2500 | Carry | Casual | Clear`,
    AimRange: ["1-3", "10", "100", "1000", "2500", "Carry", "Casual", "Clear"],
    execute(message, args) {
        // check for invalid usage
        if (args.length == 1) {
            args[0] = args[0].toLowerCase();
            args[0] = args[0].replace("c", "C");
            if (!this.AimRange.includes(args[0])) {
                return message.client.commands.get("help").execute(message,[this.name]);
            }
        } else {
            return message.client.commands.get("help").execute(message,[this.name]);
        }
        // fix input string
        for (var i = 0; i < 5; i++) {
            if (this.AimRange[i] == args[0]) {
                args[0] = "Top " + args[0];
            }
        }

        // remove all existing aim roles
        const member = message.member;
        const clearlib = require('./clearall.js');
        clearlib.clear(member).then(function () {
            if (args[0] === "Clear") {
                return message.channel.send(`Role cleared for **${message.author.username}**`);
            } else {
                const role = message.guild.roles.find('name', args[0]);
                if (role == null) {
                    return message.reply(args[0] + " Role not found!");
                }
                // add new role
                member.addRole(role).then(function (result) {
                    return message.channel.send(`**${message.author.username}** is aiming for ${role.name}, good luck!`);
                }).catch(console.error);
            }
        });
    },
};