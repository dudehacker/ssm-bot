module.exports = {
    name: 'setprofile',
    description: 'Set profile for yourself',
    args: true,
    usage: `<Aim> <Band Power> <Score Boost %> <Skill Level>`,
    execute(message, args) {
        const aimRange = [1, 2, 3, 10, 100, 1000, 2500],
            bpRange = [1, 300],
            slRange = [1, 2, 3, 4, 5],
            skillMod = [60, 100];
        var errMsg;
        const cmdInfo =
            'Parameter 1: Aim (Top X)\n' +
            'Parameter 2: Band Power (k)\n' +
            'Parameter 3: Score Boost (%)\n' +
            'Parameter 4: Skill Level\n' +
            'Example: if you are aiming for top 1000, with BP of 230k, using 100% scorer skill level 5 as leader\n'
            + '\`' + this.name + ' 1000 230 100 5' + '\`';


        if (args.length < 4) {
            errMsg = '__Missing parameter(s) for **' + this.name + '** command__\n'
            message.channel.send(errMsg + cmdInfo);
            return;

        } else if (args.length > 4) {
            errMsg = '__Too many parameter(s) for **' + this.name + '** command__\n'
            message.channel.send(errMsg + cmdInfo);
            return;
        } else {
            // try to parse the arguments
            const aim = parseInt(args[0], 10);
            if (isNaN(aim)) {
                errMsg = '__Please input a number for Top X__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            } else if (!aimRange.includes(aim)) {
                errMsg = '__Valid values for Top X are: ' + aimRange + '__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            }
            console.log('you are aiming for top ' + aim);

            const BP = parseInt(args[1], 10);
            if (isNaN(BP)) {
                errMsg = '__Please input a number for Band Power__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            } else if (BP < bpRange[0] || BP > bpRange[1]) {
                errMsg = '__Valid values for Band Power are between: ' + bpRange + '__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            }
            console.log('Your band power is  ' + BP + 'k');

            const skillpercent = parseInt(args[2], 10);
            if (isNaN(skillpercent)) {
                errMsg = '__Please input a number for Skill % Modifier__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            } else if (!skillMod.includes(skillpercent)) {
                errMsg = '__Valid values for Skill % Modifier are: ' + skillMod + '__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            }
            console.log('Your skill modifier % is ' + skillpercent);

            const sl = parseInt(args[3], 10);
            if (isNaN(sl)) {
                errMsg = '__Please input a number for Skill Level__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            } else if (!slRange.includes(sl)) {
                errMsg = '__Valid values for Skill Level are: ' + slRange + '__\n';
                message.channel.send(errMsg + cmdInfo);
                return;
            }
            console.log('Your skill level is ' + sl);

            message.channel.send('Profile is set for ' + message.author.username);
            return;
        }
    },
};