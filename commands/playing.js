module.exports = {
    name: 'playing',
    description: 'Show who is currently ready to play',
    aliases: ['ls', 'list'],
    guildOnly: true,
    msg: "There is no one playing\nGood luck with pub!",
    execute(message) {
        const role = message.guild.roles.find('name', 'Playing');
        if (role.members.array().length == 0) return message.channel.send(this.msg);

        const idle = require("../idle.js");
        idle.clearIdle(message.guild).then(
            function () {
                var output = "The following players are ready: \n";
                role.members.array().forEach(function (member) {
                    output += member.displayName + ", ";
                });
        
                return message.channel.send(output.slice(0, -2));
            }
        );

       
    }
}