const fs = require('fs');
const filename = './config.json';

module.exports = {
    name: 'ad',
    description: 'Spam ad for promoting your own room, use `!setad` to set a new ad',
    guildOnly: true,
    execute(message) {
        var config = JSON.parse(fs.readFileSync(filename, 'utf8'));
        return message.channel.send(config.ad);
    }
}