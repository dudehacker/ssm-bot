module.exports = {
	name: 'getprofile',
    description: 'Get the profile of a player',
    usage: `${exports.name} @user`,
	execute(message) {
		if (!message.mentions.users.size) {
			return message.reply('you need to tag a user in order to kick them!');
		}

		const taggedUser = message.mentions.users.first();
        var text = `Profile for ${taggedUser.username}`;
		message.channel.send(text);
	},
};