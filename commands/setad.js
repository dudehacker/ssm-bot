const fs = require('fs');
const utils = require('../utils.js');
const filename = './config.json';

module.exports = {
    name: 'setad',
    description: 'Set the last message you sent as advertisement for your room',
    guildOnly: true,
    rankerOnly: true,
    execute(message) {
        if (message.member.lastMessage != null){
            message.channel.fetchMessages({ limit: 20 })
                .then(messages => this.getLastMessage(messages,message.author))
                .catch(console.error);
        } else {
            return message.channel.send("Can't retrieve your last message");
        }
    },

    getLastMessage(messages,author){
        var msgs = messages.array();
        for (var i = 1; i < msgs.length; i++){
            if (author === msgs[i].author && !utils.isValidCommand(msgs[i].content)) {
                var config = JSON.parse(fs.readFileSync(filename, 'utf8'));
                config.ad = msgs[i].content;
                fs.writeFileSync(filename, JSON.stringify(config,null,2));
                return msgs[0].channel.send("Ad is updated");
            }
        }
    }
}