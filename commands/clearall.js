const AimRange = require('./aim.js').AimRange;

module.exports = {
    name: 'clearall',
    description: 'Clear the aim of everyone',
    adminOnly: true,
    guildOnly: true,
    execute(message) {

        message.guild.members.forEach(this.clear);
        return message.channel.send(`All members' roles are cleared!`);
    },
    clear: async function (guildMember) {
        if (!guildMember.user.bot) {
            const arr = guildMember.roles.array();
            for (var i = 0; i < arr.length; i++) {
                var r = arr[i];
                var newName = r.name.replace("Top ", "");
                if (AimRange.includes(newName)) {
                    //console.log("Deleting role: " + r.name);
                    await guildMember.removeRole(r).catch(console.error);
                }
            }
            console.log("Cleared role for " + guildMember.user.username);
        }
    }
};


