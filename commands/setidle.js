const utils = require('../utils.js');
const fs = require('fs');

module.exports = {
    name: 'setidle',
    description: 'Change idle timer',
    guildOnly: true,
    args: true,
    adminOnly: true,
    usage: `minutes`,
    execute(message, args) {
        // check argument
        if (args.length == 1 && parseInt(args[0], 10)) {
            const filename = './config.json';
            var config = JSON.parse(fs.readFileSync(filename, 'utf8'));
            config.timeout = parseInt(args[0], 10);
            fs.writeFile(filename, JSON.stringify(config,null,2), function (err) {
                if (err) return console.log(err);
                return message.channel.send("Idle timer set to: " + config.timeout + " minutes");
            });
            
        } else {
            return message.client.commands.get("help").execute(message,[this.name]);
        }
    }
}