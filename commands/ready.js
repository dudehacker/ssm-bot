module.exports = {
    name: 'ready',
    aliases: ['standby'],
    guildOnly: true,
    description: 'Let people know you\'re available. This makes you pingable with `@playing` role so we can get your attention!',
    execute(message) {
        const role = message.guild.roles.find('name', 'Playing');
        message.member.addRole(role);
        return message.channel.send(`**${message.author.username}**, It is Fuwa Fuwa Time!`);
    }

};