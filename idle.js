var fs = require('fs');

module.exports = {
    clearIdle: async function(guild) {
        var min = JSON.parse(fs.readFileSync('config.json', 'utf8')).timeout;
        //console.log(min);
        // find all users with @Playing role
        const playingRole = guild.roles.find('name', 'Playing');
        const members = playingRole.members;

        // check which user is idle
        const now = new Date();
        const timeout = new Date(now.getTime() - min*60000);
        const a = members.array();
        for (var i = 0; i<  a.length; i++ ){
            let member = a[i];
            if (member.lastMessage != null){
                /*console.log("----------------------");
                console.log(member.displayName + " last msg at " + member.lastMessage.createdAt);
                console.log(timeout);
                console.log(member.lastMessage.createdAt < timeout);*/
                if (member.lastMessage.createdAt < timeout){
                    await member.removeRole(playingRole);
                    console.log("Removed @playing role from " + member.displayName);
                }
                
            }
        }

    }
};