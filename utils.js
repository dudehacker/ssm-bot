module.exports = {
    PermissionDeniedException: "You do not have permission to use this command",
    isAdmin(member) {

        if (member.roles.find('name', 'Sub Manager') != null) {
            return true;
        }

        if (member.roles.find('name', 'Manager') != null) {
            return true;
        }

        return false;
    },
    ishigherTop10(member){
        if (this.isAdmin(member)){
            return true;
        }

        if (member.roles.find('name', 'Top 10') != null) {
            return true;
        }

        if (member.roles.find('name', 'Top 1-3') != null) {
            return true;
        }

        return false;
    },
    isValidCommand(msg){
        return msg.search(/![A-Za-z]+/) != -1
    }
}